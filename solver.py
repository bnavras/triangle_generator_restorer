import time
import numpy as np
from argparse import ArgumentParser
from collections import defaultdict


class ShapeVertexDetector:
    def __init__(self, shape, keypoint_proximity_threshold=1, caching_threshold=30, max_average_noise=60):
        self.shape = shape
        self.max_average_noise = max_average_noise
        self.caching_threshold = caching_threshold
        self.max_keypoint_proximity = keypoint_proximity_threshold

    def detect(self, image):
        horisont_keypoins, vertical_keypoins = self._find_keypoins(image)

        horisont_lines = self._build_lines(image, horisont_keypoins)
        vertical_lines = self._build_lines(image, vertical_keypoins)

        probable_lines = sorted(horisont_lines.items(), key=lambda x: x[1], reverse=True)[:self.shape - 1]
        probable_lines += sorted(vertical_lines.items(), key=lambda x: x[1], reverse=True)[:self.shape - 1]
        selected_lines = sorted(probable_lines, key=lambda x: x[1], reverse=True)[:self.shape]

        shape_points = []
        for i, (line, keypoint_counts) in enumerate(selected_lines):
            try:
                vertex = self._line_intersection(line, selected_lines[(i+1) % len(selected_lines)][0])
                shape_points.append(vertex)
            except Exception as error:
                print(error.args)
                continue

        return shape_points

    def _find_keypoins(self, image):
        empty_column = np.zeros((image.shape[0], 1))
        vertical_bordered_image = np.hstack([empty_column, image, empty_column])
        horizontal_keypoints = np.where((image[:, :-1] + image[:, 1:] == 255) &
                                        (vertical_bordered_image[:, :-3] + vertical_bordered_image[:, 3:] == 0))

        empy_row = np.zeros((1, image.shape[1]))
        horizontal_bordered_image = np.vstack([empy_row, image, empy_row])
        vertical_keypoints = np.where((image[:-1, :] + image[1:, :] == 255) &
                                      (horizontal_bordered_image[:-3, :] + horizontal_bordered_image[3:, :] == 0))

        return (
            list(zip(horizontal_keypoints[1], horizontal_keypoints[0])),
            list(zip(vertical_keypoints[1], vertical_keypoints[0]))
        )

    def _specify_line_coordinates(self, image, x1, y1, x2, y2):
        steep = abs(y2 - y1) > abs(x2 - x1)
        delta0 = 1 - image[y1, x1] / 255
        delta1 = 1 - image[y2, x2] / 255

        if steep:
            x1 += delta0
            x2 += delta1
        else:
            y1 += delta0
            y2 += delta1
        return (x1, y1), (x2, y2)

    def _is_wu(self, image, x1, y1, x2, y2):
        steep = abs(y2 - y1) > abs(x2 - x1)
        if steep:
            return (x1 + 1) < image.shape[1] and (image[y1, x1] + image[y1, x1 + 1]) == 255
        else:
            return (y1 + 1) < image.shape[0] and (image[y1, x1] + image[y1 + 1, x1]) == 255

    def _build_lines(self, image, keypoins):
        noise_level = min(image.mean(), self.max_average_noise) / self.max_average_noise
        distance_threshold = 100*0.95*(1 - noise_level)
        lines, unused_points = defaultdict(int), set(keypoins)

        for i, point_a in enumerate(keypoins):
            for point_b in unused_points.difference(keypoins[:i + 1]):
                if not self._is_wu(image, *point_a, *point_b) or calc_distance(*point_a, *point_b) < distance_threshold: continue

                specified_a, specified_b = self._specify_line_coordinates(image, *point_a, *point_b)
                line = Line(*specified_a, *specified_b)
                line_points = set()

                for point in unused_points:
                    if point != point_a and point != point_b and line.distance(*point) < self.max_keypoint_proximity:
                        lines[(specified_a, specified_b)] += 1
                        line_points.add(point)
                if lines[(specified_a, specified_b)] > self.caching_threshold:
                    unused_points = unused_points.difference(line_points)

        return lines

    def _line_intersection(self, line1, line2):
        x_differences = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
        y_differences = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

        divisor = self._det(x_differences, y_differences)
        if divisor == 0:
            raise Exception('Lines do not intersect')

        lines_determinants = (self._det(*line1), self._det(*line2))
        x = self._det(lines_determinants, x_differences) / divisor
        y = self._det(lines_determinants, y_differences) / divisor

        return x, y

    def _det(self, a, b):
        return a[0] * b[1] - a[1] * b[0]


class WuLineDrawer:
    def _draw(self, image, x, y, brightness):
        image[int(y), int(x)] = round(255 * brightness)

    def _fractional_part(self, x):
        return x - x // 1.0

    # TODO: pure function
    def draw_line(self, image, x1, y1, x2, y2):
        steep = abs(y2 - y1) > abs(x2 - x1)

        if steep:
            x1, y1 = y1, x1
            x2, y2 = y2, x2
        if x1 > x2:
            x1, x2 = x2, x1
            y1, y2 = y2, y1

        dx = x2 - x1
        dy = y2 - y1
        gradient = dy / dx
        if dx == 0.0:
            gradient = 1.0

        interpolated_y = y1 + gradient * (round(x1) - x1)
        x_pixel1, x_pixel2 = round(x1), round(x2)

        # TODO: remove one loop
        if steep:
            for x in range(int(x_pixel1), int(x_pixel2) + 1):
                self._draw(image, interpolated_y // 1.0, x, 1 - self._fractional_part(interpolated_y))
                self._draw(image, interpolated_y // 1.0 + 1, x, self._fractional_part(interpolated_y))
                interpolated_y = interpolated_y + gradient
        else:
            for x in range(int(x_pixel1), int(x_pixel2) + 1):
                self._draw(image, x, interpolated_y // 1.0, 1 - self._fractional_part(interpolated_y))
                self._draw(image, x, interpolated_y // 1.0 + 1, self._fractional_part(interpolated_y))
                interpolated_y = interpolated_y + gradient

    def draw_triangle(self, image, points):
        for i, point_a in enumerate(points):
            point_b = points[(i + 1) % points.shape[0]]
            self.draw_line(image, *point_a, *point_b)

        return image


class TriangleGenerator:
    def __init__(self, min_line_length, min_angle, max_coordinate_value):
        self.min_line_length = min_line_length
        self.min_angle = min_angle
        self.max_coordinate_value = max_coordinate_value

    def generate_triangle(self):
        while True:
            points = np.random.uniform(0, self.max_coordinate_value, (3, 2))
            lengths = self._get_lengths(points)
            angles = self._get_angles(lengths)
            if all(map(lambda length: length >= self.min_line_length, lengths)) and \
               all(map(lambda angle: angle >= self.min_angle, angles)):
                return points

    def _get_lengths(self, points):
        lengths = list()

        for i, point_a in enumerate(points):
            point_b = points[(i + 1) % points.shape[0]]
            length = ((point_a[0] - point_b[0]) ** 2 + (point_a[1] - point_b[1]) ** 2) ** 0.5
            lengths.append(length)

        return lengths

    def _get_angles(self, lengths):
        angles = list()

        for i, b in enumerate(lengths):
            c, a = lengths[(i + 1) % len(lengths)], lengths[(i + 2) % len(lengths)]
            cos_a = (b ** 2 + c ** 2 - a ** 2) / (2 * b * c)
            angle = np.arccos(cos_a) * 180 / np.pi
            angles.append(angle)

        return angles


class PortableFormatIO:
    def _read_header(self, pgm_file):
        version, shape, maximum_value = None, None, None
        for line in pgm_file:
            line = line.decode()
            if line.startswith('#'):
                continue
            elif not version:
                version = line.strip()
            elif not shape:
                shape = list(map(int, line.strip().split()))
            elif not maximum_value:
                maximum_value = int(line.strip())
                return version, shape, maximum_value

    def read_pgm(self, image_path):
        with open(image_path, 'rb') as pgm:
            version, shape, max_pixel = self._read_header(pgm)
            if version == 'P5':
                pixels = [value for line in pgm for value in bytearray(line)]
                return np.array(pixels).reshape(shape)
            if version == 'P2':
                image = [list(map(int, line.split())) for line in pgm]
                return np.array(image)

    def write_pgm(self, image, image_path='image.pgm'):
        with open(image_path, 'w') as pgm:
            pgm.write('P2\n')
            pgm.write(' '.join(map(str, image.shape)) + '\n')
            pgm.write(str(image.max()) + '\n')

            for row in image:
                line = ' '.join(map(str, row))
                pgm.write(line + '\n')


class Line:
    def __init__(self, x1, y1, x2, y2):
        self.A = y1 - y2
        self.B = x2 - x1
        self.C = x1 * y2 - x2 * y1
        assert self.A != 0 or self.B != 0

    def distance(self, x, y):
        return abs(self.A * x + self.B * y + self.C) / ((self.A ** 2 + self.B ** 2) ** 0.5)


# utils
def add_noise(image, noise_probability=0.1):
    probabilities = np.random.rand(*image.shape)
    noise = np.random.randint(0, 255, image.shape)
    mask = probabilities < noise_probability
    image[mask] = noise[mask]
    return image


def write_points(points):
    with open('output.txt', 'w') as output_file:
        for point in points:
            output_file.write('{} {}\n'.format(*point))


def calc_loss(target, predict):
    distance1 = min([calc_distance(*target[0], *point) for point in predict])
    distance2 = min([calc_distance(*target[1], *point) for point in predict])
    distance3 = min([calc_distance(*target[2], *point) for point in predict])
    return max(min(distance1, 5), min(distance2, 5), min(distance3, 5))


def calc_distance(x1, y1, x2, y2):
    return ((x2-x1)**2 + (y2-y1)**2)**0.5


def run_test(test_count, noise_probability=0.1):
    detector = ShapeVertexDetector(shape=3)
    drawer = WuLineDrawer()
    triangle_generator = TriangleGenerator(
        min_line_length=100,
        min_angle=30,
        max_coordinate_value=500
    )

    losses = []
    times = []

    for i in range(test_count):
        start_time = time.time()
        image = np.zeros((500, 500), dtype=int)

        triangle_coordinates = triangle_generator.generate_triangle()
        image = drawer.draw_triangle(image, triangle_coordinates)
        add_noise(image, noise_probability)

        predicted_points = detector.detect(image)
        loss = calc_loss(triangle_coordinates, predicted_points)

        losses.append(loss)
        times.append(time.time() - start_time)

    print('AVG LOSS: ', round(sum(losses) / len(losses), 3), end='; ')
    print('MIN LOSS: ', round(min(losses), 3), end='; ')
    print('MAX LOSS: ', round(max(losses), 3))

    print('AVG TIME: ', round(sum(times) / len(times), 3), end='; ')
    print('MIN TIME: ', round(min(times), 3), end='; ')
    print('MAX TIME: ', round(max(times), 3))


if __name__ == '__main__':
    argument_parser = ArgumentParser()
    argument_parser.add_argument('-generate',
                                 type=float,
                                 dest='noise_probability',
                                 help='Generates a triangle image and saves to image.pgm')
    argument_parser.add_argument('-restore',
                                 type=str,
                                 dest='image_path',
                                 help='Restores the coordinates of the triangle vertices in the image')
    parsed_argument = argument_parser.parse_args()

    pgm_io = PortableFormatIO()
    if parsed_argument.noise_probability is not None:
        drawer = WuLineDrawer()
        triangle_generator = TriangleGenerator(
            min_line_length=100,
            min_angle=30,
            max_coordinate_value=500
        )

        triangle_coordinates = triangle_generator.generate_triangle()
        image = np.zeros((500, 500), dtype=int)
        image = drawer.draw_triangle(image, triangle_coordinates)

        add_noise(image, parsed_argument.noise_probability)
        pgm_io.write_pgm(image)
        print('The triangle was successfully generated and saved as image.pgm')

    if parsed_argument.image_path is not None:
        shape = 3
        image = pgm_io.read_pgm(parsed_argument.image_path)
        detector = ShapeVertexDetector(shape=shape)

        predicted_points = detector.detect(image)
        null_points = [(0.0, 0.0) for _ in range(shape - len(predicted_points))]
        write_points(predicted_points + null_points)

        print('The result was successfully written to output.txt')
